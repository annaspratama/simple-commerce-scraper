# from rest_framework.renderers import JSONRenderer
# from rest_framework.utils import json
from rest_framework import status
from rest_framework.response import Response


class JSONResponseRenderer():
    """ JSON renderer """

    def render(**kwargs):
        return Response(
            {
                'message': kwargs.get('msg', None),
                'data': kwargs.get('data', None)
            },
            status=kwargs.get('code', status.HTTP_200_OK)
        )


# class JSONResponseRenderer(JSONRenderer):
#     """ JSON renderer """
#     # media_type = 'text/plain'
#     # media_type = 'application/json'
#     charset = 'utf-8'

#     def render(self, data, accepted_media_type=None, renderer_context=None):
#         response_dict = {
#             'status': "failure",
#             'message': "",
#             'data': data,
#         }
#         data = response_dict
#         return json.dumps(data)