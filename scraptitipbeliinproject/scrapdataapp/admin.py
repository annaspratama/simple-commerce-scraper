from django.contrib import admin
from scrapdataapp.models import Product, Photo

# Register your models here.
admin.site.register(Product)
admin.site.register(Photo)