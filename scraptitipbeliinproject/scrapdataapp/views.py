import requests
import time
from rest_framework import viewsets, status
from .serializers import ProductSerializer
from .models import Photo, Product
from urllib.parse import urlparse
from scraptitipbeliinproject.renderer import JSONResponseRenderer
from bs4 import BeautifulSoup
from selenium import webdriver


# Create your views here.
class ProductViewSet(viewsets.ModelViewSet):
    """ Manage product object """
    
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    AMAZON = "www.amazon.com"
    EBAY = "www.ebay.com"

    def list(self, request):
        """ Get products """
        
        serializer = self.serializer_class(self.queryset, many=True)
        return JSONResponseRenderer.render(msg="Retrieved", data=serializer.data, code=status.HTTP_200_OK)

    def create(self, request):
        """ Create product """
        
        serializer = self.serializer_class(data=request.data)
        
        if serializer.is_valid():
            url = serializer.validated_data.get('source_url')
            domain = urlparse(url).netloc
            if domain == self.EBAY:
                scrap_data = self.scrap_ebay(url=url)
            elif domain == self.AMAZON:
                scrap_data = self.scrap_amazon(url=url)
            else:
                scrap_data = None

            if scrap_data is None:
                return JSONResponseRenderer.render(msg="Unsupported commerce website", code=status.HTTP_500_INTERNAL_SERVER_ERROR)

            product = serializer.save(
                name=scrap_data['title'],
                price=scrap_data['price'],
            )
            photos = scrap_data['images']
            
            for photo in photos:
                Photo.objects.create(product=product, **photo)
            
            return JSONResponseRenderer.render(msg="Created", data=serializer.data, code=status.HTTP_201_CREATED)
        return JSONResponseRenderer.render(msg=serializer.errors, code=status.HTTP_400_BAD_REQUEST)

    def scrap_ebay(self, url):
        """ Scrap data from www.ebay.com """

        soup = self.__scrap_response_bs(url=url)
        data = self.__get_data_ebay(soup)
        return data

    def scrap_amazon(self, url):
        """ Scrap data from www.amazon.com """

        soup = self.__scrap_response_selenium(url=url)
        data = self.__get_data_amazon(soup)
        return data

    def __scrap_response_bs(self, url):
        """ Return soup response """

        headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0'}
        res = requests.get(url=url, headers=headers)
        
        if not res.ok:
            return JSONResponseRenderer.render(msg=res.status_code, code=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            soup = BeautifulSoup(res.text, 'lxml')
            return soup

    def __scrap_response_selenium(self, url):
        """ Return soup response """

        driver = webdriver.Remote("http://selenium:4444/wd/hub")
        # driver.maximize_window()
        driver.get(url)
        time.sleep(5)
        content = driver.page_source.encode('utf-8').strip()
        soup = BeautifulSoup(content, 'html.parser')
        return soup

    def __get_data_ebay(self, soup):
        """ Parse HTML & get the data from ebay """

        try:
            title = soup.find("h1", id="itemTitle").text
            title = title.split("about")[1].strip()
            price = soup.find("span", id="prcIsum").text
            currency, price = price.split(" ")
            price = filter(str.isdigit, price)
            price = "".join(price)
            price = float(price)/100
            image = soup.find("img", id="icImg").get("src")
        except:
            raise TypeError("Server error")

        imgs = []
        img = {'photo_url': image}
        imgs.append(img)
        data = {
            'title': title,
            'price': price,
            'currency': currency,
            'images': imgs
        }
        return data

    def __get_data_amazon(self, soup):
        """ Parse HTML & get the data from amazon """

        try:
            title = soup.find("h1", id="title")
            if title is not None:
                title = title.select_one("span", id="productTitle").text
                title = title.strip()

            price = soup.find("span", id="price_inside_buybox")
            if price is not None:
                price = price.text
                price = filter(str.isdigit, price)
                price = "".join(price)
                price = float(price)/100

            image = soup.find("div", id="imgTagWrapperId")
            if image is not None:
                image = image.select_one("img", class_="a-dynamic-image").get("src")
        except:
            raise TypeError("Server error")

        imgs = []
        img = {'photo_url': image}
        imgs.append(img)
        data = {
            'title': title,
            'price': price,
            'images': imgs
        }
        return data