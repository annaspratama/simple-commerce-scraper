from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

app_name = 'scrapdataapp'
router = DefaultRouter()
router.register('product', views.ProductViewSet, basename='product')

urlpatterns = [
    path('v1/', include(router.urls))
]