from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from scrapdataapp.serializers import ProductSerializer
from scrapdataapp.models import Product, Photo
from rest_framework import status
from scrapdataapp.views import ProductViewSet

PRODUCTS_URL = reverse('scrapdataapp:product-list')
COMMERCE_EBAY_URL = "https://www.ebay.com/itm/124893349756?_trkparms=amclksrc%3DITM%26aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20180816085401%26meid%3De73f51e0546440f2ab0bb0431c4efa35%26pid%3D100970%26rk%3D3%26rkt%3D3%26sd%3D255107476973%26itm%3D124893349756%26pmt%3D1%26noa%3D1%26pg%3D2380057%26brand%3DApple&_trksid=p2380057.c100970.m5481&_trkparms=pageci%3A0151f98e-7f30-11ec-82e4-1eb464922a1f%7Cparentrq%3A99f4e21517e0a120a88d5881ffffb54a%7Ciid%3A1"
COMMERCE_AMAZON_URL = "https://www.amazon.com/Apple-MacBook-Display-MPXQ2LL-Refurbished/dp/B078GX9R5W/ref=sr_1_1?keywords=macbook&qid=1643276059&sr=8-1"


class ProductApiTests(TestCase):
    """ Test the product API """

    def setUp(self):
        self.client = APIClient()

    def create_product(self, **params):
        """ Create product object """

        data = {
            'name': "iPhone 13 Pro Max",
            'price': 10.00,
            'source_url': "https://www.ebay.com/"
        }
        data.update(params)
        product = Product.objects.create(**data)
        self.create_photo(product=product)
        return product

    def create_photo(self, product):
        """ Create photo object """

        data = [
            {'photo_url': "https://cdn-brilio-net.akamaized.net/community/2018/12/24/15804/image_1545475036_5c1e13dce178b.jpg"},
            {'photo_url': "https://cdn-brilio-net.akamaized.net/community/2018/12/24/15804/image_1545475036_5c1e13dce178b.jpg"},
            {'photo_url': "https://cdn-brilio-net.akamaized.net/community/2018/12/24/15804/image_1545475036_5c1e13dce178b.jpg"},
            {'photo_url': "https://cdn-brilio-net.akamaized.net/community/2018/12/24/15804/image_1545475036_5c1e13dce178b.jpg"},
            {'photo_url': "https://cdn-brilio-net.akamaized.net/community/2018/12/24/15804/image_1545475036_5c1e13dce178b.jpg"},
        ]
        for dt in data:
            Photo.objects.create(product=product, **dt)
        return Photo.objects.filter(product=product)

    def test_scrap_ebay(self):
        """ Test scrap ebay """

        product_viewset = ProductViewSet()
        scrap_ebay = product_viewset.scrap_ebay(url=COMMERCE_EBAY_URL)
        data = {
            'title',
            'price',
            'currency',
            'images'
        }
        for key in scrap_ebay.keys():
            self.assertIn(key, data)

    def test_scrap_amazon(self):
        """ Test scrap amazon """

        product_viewset = ProductViewSet()
        scrap_ebay = product_viewset.scrap_amazon(url=COMMERCE_EBAY_URL)
        data = {
            'title',
            'price',
            'images'
        }
        for key in scrap_ebay.keys():
            self.assertIn(key, data)

    def test_retrieve_products(self):
        """ Test retrieve products """

        self.create_product()
        res = self.client.get(PRODUCTS_URL)
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data['data'], serializer.data)

    def test_scrap_product(self):
        """ Test get scraped product """

        payload = {
            'source_url': COMMERCE_EBAY_URL
        }
        res = self.client.post(PRODUCTS_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)