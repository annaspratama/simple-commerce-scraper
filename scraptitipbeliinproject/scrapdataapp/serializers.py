from rest_framework import serializers
from scrapdataapp.models import Product, Photo


class PhotoSerializer(serializers.ModelSerializer):
    """ Serialize to photo object """

    class Meta:
        model = Photo
        fields = ('product', 'photo_url')
        read_only_fields = ('id',)


class ProductSerializer(serializers.ModelSerializer):
    """ Serialize to product object """

    # photos = PhotoSerializer(many=True, read_only=True)
    photos = serializers.StringRelatedField(
        many=True,
        read_only=True
        # queryset=Photo.objects.all()
    )

    class Meta:
        model = Product
        fields = '__all__'
        read_only_fields = ('id',)
        extra_kwargs = {
            'name': {'read_only': True},
            'price': {'read_only': True},
        }