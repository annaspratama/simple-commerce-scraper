from django.apps import AppConfig


class ScrapdataappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'scrapdataapp'
