from django.db import models


# Create your models here.
class Product(models.Model):
    """ Source of product data """

    name = models.CharField(max_length=300)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    source_url = models.URLField(max_length=2000)

    def __str__(self):
        return self.name


class Photo(models.Model):
    """ Photo belong to product """

    product = models.ForeignKey('scrapdataapp.Product', related_name='photos', on_delete=models.CASCADE)
    photo_url = models.URLField()

    def __str__(self):
        return self.photo_url