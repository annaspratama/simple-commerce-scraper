FROM python:latest

ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

WORKDIR /scraptitipbeliinproject
COPY ./scraptitipbeliinproject/ /scraptitipbeliinproject

#Heroku deployment
CMD gunicorn --bind 0.0.0.0:$PORT scraptitipbeliinproject.wsgi:application
# CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "scraptitipbeliinproject.wsgi:application"]